const dev = {
    appPort: 3000,
    socketPort: 8080,
    wss: 'http://localhost:8080',
    logService: {
        type: 'local'
    },
    auctionService: {
        type: 'local'
    },
    userService: {
        type: 'local'
    },
    bidService: {
        type: 'local'
    },
    mongoDb: {
        mongoUser: 'dbuser',
        mongoPassword: 'Z!CS8srDx4cPv_X',
        mongoPath: 'ds115740.mlab.com:15740/occon'
    }
}

const prod = {
    appPort: 80,
    socketPort: 8080
}


module.exports = {
    dev,
    prod
}