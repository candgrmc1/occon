const mongoose = require('mongoose')
module.exports = ({
    config,
    AuctionModel,
    BidModel,
    log,
    transformer,
    bidService
}) => {
    return {
        path: '/:id/get-bids',
        method: 'get',
        handle: async (req,res) => {
            const id = req.params.id
            const bids =  await bidService.getSortedByAuctionId(id)
            log.print(bids)

            res.json(bids)
        }
    }
}