module.exports = ({
    config,
    AuctionModel,
    BidModel,
    log,
    transformer,
    services:{
        auctionService,
        bidService
    }
}) => {
    const path = '/auction'
    return {
        path: '/',
        method: 'get',
        prefix: '/auctions',
        children:[
            require('./getBids')({
                config,
                AuctionModel,
                log,
                transformer,
                bidService
            })
        ],
        handle: async (req,res) => {
            const auctions = await auctionService.getAuctions()
            res.json(transformer.success(auctions))
        }
    }
}