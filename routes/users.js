module.exports = ({
    config,
    UserModel,
    log,
    transformer,
    services:{
        userService
    }
}) => {

    return {
        path: '/users',
        method: 'get',
        handle: async (req,res) => {
            const users = await userService.getUsers()
            res.json(transformer.success(users))
        }
    }
}