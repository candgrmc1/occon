module.exports = ({
    config,
    services,
    models,
    transformer,
    socket
}) => {
    const {
        logService: log
    } = services
    const {
        UserModel,
        AuctionModel,
        BidModel
    } = models
    
    const routes = [
        require('./auctions')({
            config,
            AuctionModel,
            BidModel,
            log,
            services,
            transformer
        }),
        require('./home')({
            config,
            services
        }),
        require('./users')({
            config,
            UserModel,
            log,
            transformer,
            services
        }),
    ]
    return routes
}