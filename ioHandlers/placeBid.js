module.exports = ({
    config,
    services:{
        logService:log,
        bidService
    }
}) => {
    return {
        handle: async (data,socket,{broadcastHighestBid}) => {
            await bidService.placeBid(data)
            await broadcastHighestBid.handle()

            
        }
    }
}