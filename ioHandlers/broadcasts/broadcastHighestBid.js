module.exports = ({
    config,
    services:{
        logService:log,
        bidService
    },
    models:{
        BidModel,
        UserModel
    },
    io
}) => {
    return {
        handle: async () => {
            log.success('broadcasting')
            const highestBid = await bidService.getHighestBid()
            io.sockets.emit('broadcast',{highestBid})
        }
    }
}