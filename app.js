const env = process.APP_ENV || 'dev'
const config = require('./config')[env]

const app = require('express')();
var cors = require('cors')

app.use(cors())
const http = require('http').createServer(app)

const {
    createApp,
    createSocket,
    transformer
} = require('./lib')



class App {

    async run() {
        const {mongoDb} = config
        const models = require('./schemas')({
            config: mongoDb
        })
        const services = require('./services')({
            config,
            models
        })
                
        await createApp({
            render: {
                server: {
                    app,
                    http,
                    transformer
                },
                socket: {
                    ...await createSocket({
                        config,
                        services,
                        models
                    })
                }
            },
            network: {
                services
            },
            db: {
                models
            },
            config
        })
    }
}

module.exports = App