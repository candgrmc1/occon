const App = require('./app')
const app = new App()

app.run().then().catch((e)=>{
    console.log('terminating..')
    throw new Error(e)
})