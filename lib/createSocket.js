
module.exports = async ({
    config,
    services,
    models
}) => {
    const {socketPort} = config
    const {logService:log} = services

    const io = require('socket.io').listen(socketPort)
    log.success(`socket is running on ${socketPort}`)
    const handlers = require('../ioHandlers')({
        config,
        services,
        models,
        io
    })
    const broadcastHighestBid = require('../ioHandlers/broadcasts/broadcastHighestBid')({config,services,models,io})
    
    

    io.sockets.on('connect',async (socket)=>{
        log.success('client connected')

        await broadcastHighestBid.handle()
        
        for(let i in handlers){
            socket.on(i,a => handlers[i].handle(a,socket,{broadcastHighestBid}))
        }
        socket.on('disconnect',() => {
        log.warn('socket::client disconnected')
    })
    })
    
    return {io}
    

}