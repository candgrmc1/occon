module.exports = ({
    services,
    models,
    config
}) => {
    const configuredServices = {}
    for(let s of services ){
        const serviceConfig = config[s]
        
        if(serviceConfig == undefined){
            throw new Error('configration-error::missing-service-config')
        }
        const path = `../services/${s}`
        
        const {
            type
        } = serviceConfig
        if(type == 'local'){
            const instance = require(path)
            const protos = Object.getOwnPropertyNames(instance.prototype)
            for( let prototype of protos ){
                switch(prototype){
                    case 'log':
                        let log = require('../services/logService')
                        instance.prototype.log = new log()
                    break
                    case 'db':
                        for(let index in instance.prototype.db){
                            instance.prototype.db[index] = models[index]
                        }
                        break;
                    case 'config':
                        instance.prototype.config = serviceConfig
                        break

                    default:
                        break
                }
            }
            configuredServices[s] = new instance()
        }

        
    }
    return configuredServices
}