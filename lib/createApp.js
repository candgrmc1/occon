const router = require('express').Router();
module.exports = async ({
    render: {
        server: {
            app,
            http,
            transformer
        },
        socket: {
            socket
        }
    },
    network: {
        services
    },
    db: {
        models
    },
    config,
}) => {
    const {
        appPort,
        wss
    } = config
    const {
        logService: log
    } = services
    const routes = require('../routes')({
        config,
        services,
        models,
        transformer,
        socket
    })

    for (let route of routes) {
        const {
            path,
            handle,
            method,
            prefix,
            children
        } = route
        if (prefix == undefined) {
            app[method](path, async (req, res) => await handle(req, res))
        } else {
            app.use(prefix, await handleChildren({
                children,
                basePath: path,
                baseHandler: handle,
                baseMethod: method
            }))
        }
    }

    http.listen(appPort, (req, res) => {
        log.success(`create-app::app is running on ${appPort}`)
    })


  

    async function handleChildren({
        children,
        basePath,
        baseHandler,
        baseMethod
    }) {

        children.map((i) => {
            const {
                path,
                handle,
                method
            } = i
            router[method](path, async (req, res) => await handle(req, res))

        })
        router[baseMethod](basePath, async (req, res) => await baseHandler(req, res))
        return router
    }


}