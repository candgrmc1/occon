const Reset = "\x1b[0m"
const FgGreen = "\x1b[32m"
const FgRed = "\x1b[31m"
class logService {
    
    warn = (message) => console.warn( message)
    error = (message) => console.error(FgRed, message, Reset)
    trace = (message) => console.trace( message)
    print = (message) => console.log(message)
    success = (message) => console.log( FgGreen,  message , Reset)
}

module.exports = Object.seal(logService)