class bidService {
    
    async getSortedByAuctionId(id){
         const bids = await this.db.BidModel.find({auctionId: id}).sort({createdAt:-1})
         return bids
    }

    async getHighestBid(){
        const highestBid = await this.db.BidModel.findOne().sort({amount:-1})
        const user = await this.db.UserModel.findById(highestBid.userId);
        return {
            amount: highestBid.amount,
            user,
            time: highestBid.createdAt
        }
    }
    async placeBid({auctionId, userId, amount}){
        const bid = new this.db.BidModel()
        bid.auctionId = auctionId
        bid.userId = userId
        bid.amount = amount
        this.log.print(bid)
        await bid.save()
        
    }
 }
 bidService.prototype.log = null
 bidService.prototype.config = null
 bidService.prototype.db = {
     'BidModel': null,
     'UserModel': null
 }
 module.exports = Object.freeze(bidService)