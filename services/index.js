const {configureServices} = require('../lib')
module.exports = ({
    config,
    models
}) => {
    const services = [
        'logService',
        'auctionService',
        'userService',
        'bidService'
    ]
    return configureServices({config,services,models})
    
}

