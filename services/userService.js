class userService {
    
   async getUsers(){
        const users = await this.db.UserModel.find()
        return users
   }
}
userService.prototype.log = null
userService.prototype.config = null
userService.prototype.db = {
    'UserModel': null
}
module.exports = Object.freeze(userService)