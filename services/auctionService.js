
class auctionService {
    
   async getAuctions(){
        const auctions = await this.db.AuctionModel.find()
        return auctions
   }
}
auctionService.prototype.log = null
auctionService.prototype.config = null
auctionService.prototype.db = {
    'AuctionModel': null
}
module.exports = Object.freeze(auctionService)