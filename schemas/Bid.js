
module.exports = ({mongoose}) => {
    const Schema = mongoose.Schema;

    const bidSchema = new Schema({
       
        amount:{
            type: Number
        },
        auctionId: {
            type: String,
        },
        userId: {
            type: String,
        },
        createdAt: {
            type: Date
        }
    }, {
        timestamps: true
    });
    bidSchema.index({
        auctionId:1
    })
    return bidSchema
    
}

