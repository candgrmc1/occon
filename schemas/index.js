const mongoose = require('mongoose');
module.exports =  ({
    config
}) => {
    const {
        mongoUser,
        mongoPassword,
        mongoPath
    } = config
    try{
        mongoose.connect(`mongodb://${mongoUser}:${mongoPassword}@${mongoPath}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
            })
       
        mongoose.model('User', require('./User')({mongoose}))  
        mongoose.model('Auction', require('./Auction')({mongoose}))
        mongoose.model('Bid', require('./Bid')({mongoose,UserModel: require('./User')({mongoose}),
        AuctionModel: require('./Auction')({mongoose})}))
        
        return {
            UserModel: mongoose.model('User'),
            AuctionModel: mongoose.model('Auction'),
            BidModel: mongoose.model('Bid'),
        }
    }catch(e){
        throw new Error(e)
    }
}