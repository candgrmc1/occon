module.exports = ({mongoose}) =>{
    const Schema = mongoose.Schema;

    const auctionSchema = new Schema({
       
        name: {
            type: String
        },
        basePrice:{
            type: Number
        },
        createdAt: {
            type: Number
        },
        deadline: {
            type: Number
        }
    });

    return auctionSchema
    
   
}
